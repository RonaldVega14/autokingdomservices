autokingdomservices
This version is compatible with AndroidX

**Auto Kingdom Services**

This is a flutter app with simple animations in transitions.

Custom widgets used in details page and multiple lists embedded.

This app version is compatible with AndroidX
This is a flutter app using a simple Provider with change notifier and custom animations for the splash screen.

**Home Page**

<img src="screenshots/MainScreen.png" height = "400">


**Details Page**
Images in details page are inside a Carousel

<img src="screenshots/DetailsScreen.png" height = "400">
<img src="screenshots/DetailsImages.png" height = "400">


**Details Information Page**
Each line of details are inside a horizontal ListView

<img src="screenshots/DetailsBottomInfo.png" height = "400">
<img src="screenshots/DetailsBottomLists.png" height = "400">