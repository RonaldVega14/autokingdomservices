import 'package:flutter/material.dart';
import 'package:car_services/bloc/state_bloc.dart';
import 'package:car_services/bloc/state_provider.dart';
import 'package:car_services/model/car.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:qr_flutter/qr_flutter.dart';

//Obteniendo informacion del primer carro en la lista
Car currentCar = carList.cars[0];
bool fav = false;

class ServicePage extends StatelessWidget {
  const ServicePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        leading: Container(
          margin: EdgeInsets.only(left: 25.0),
          child: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
        ),
        actions: <Widget>[
          ActionsAppBar(),
        ],
      ),
      backgroundColor: Colors.black,
      //El body de la pantalla inicial
      body: Stack(
        children: <Widget>[
          CarDetailsAnimation(),
          CustomBottomSheet(),
          UsePromotionButton(),
        ],
      ),
    );
  }
}

class ActionsAppBar extends StatefulWidget {
  ActionsAppBar({Key key}) : super(key: key);

  @override
  _ActionsAppBarState createState() => _ActionsAppBarState();
}

class _ActionsAppBarState extends State<ActionsAppBar>
    with TickerProviderStateMixin {
  AnimationController fadeController;
  Animation fadeAnimation;
  Icon icon;

  @override
  void initState() {
    super.initState();
    icon = Icon(Icons.favorite_border);
    fadeController =
        AnimationController(duration: Duration(milliseconds: 180), vsync: this);
    fadeAnimation = Tween(begin: 0.0, end: 1.0).animate(fadeController);
  }

  forward() {
    fadeController.forward();
    stateBloc.toggleFavorite();
  }

  reverse() {
    fadeController.reverse();
    stateBloc.toggleFavorite();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<Object>(
        initialData: StateProvider().isFav,
        stream: stateBloc.favoriteStatus,
        builder: (context, snapshot) {
          return GestureDetector(
            onTap: () {
              fadeController.isCompleted ? reverse() : forward();
            },
            child: Container(
              margin: EdgeInsets.only(right: 25, top: 15),
              child: Stack(
                overflow: Overflow.visible,
                alignment: Alignment.topRight,
                children: <Widget>[
                  Icon(Icons.favorite_border),
                  FadeTransition(
                      opacity: fadeAnimation, child: Icon(Icons.favorite)),
                ],
              ),
            ),
          );
        });
  }
}

//Informacion del carro.(nombres e imagenes)
class CarDetailsAnimation extends StatefulWidget {
  @override
  _CarDetailsAnimationState createState() => _CarDetailsAnimationState();
}

class _CarDetailsAnimationState extends State<CarDetailsAnimation>
    with TickerProviderStateMixin {
  AnimationController fadeController;
  AnimationController scaleController;

  Animation fadeAnimation;
  Animation scaleAnimation;

  @override
  void initState() {
    super.initState();

    fadeController =
        AnimationController(duration: Duration(milliseconds: 180), vsync: this);
    scaleController =
        AnimationController(duration: Duration(milliseconds: 350), vsync: this);

    fadeAnimation = Tween(begin: 0.0, end: 1.0).animate(fadeController);
    scaleAnimation = Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: scaleController,
        curve: Curves.easeInOut,
        reverseCurve: Curves.easeInOut));
  }

  forward() {
    scaleController.forward();
    fadeController.forward();
  }

  reverse() {
    scaleController.reverse();
    fadeController.reverse();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<Object>(
        initialData: StateProvider().isAnimating,
        stream: stateBloc.animationStatus,
        builder: (context, snapshot) {
          snapshot.data ? forward() : reverse();
          return ScaleTransition(
            scale: scaleAnimation,
            child: FadeTransition(
              opacity: fadeAnimation,
              child: carDetails(),
            ),
          );
        });
  }

  Widget carDetails() {
    return Container(
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Container(
          padding: EdgeInsets.only(left: 25),
          child: _carTitle(),
        ),
        Container(width: double.infinity, child: CarCarousel())
      ]),
    );
  }

//Widget de la informacion
  _carTitle() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        RichText(
          text: TextSpan(
              style: TextStyle(color: Colors.white, fontSize: 38),
              children: [
                TextSpan(text: currentCar.companyName + '\n'),
                TextSpan(
                    text: currentCar.carName,
                    style: TextStyle(fontWeight: FontWeight.w700)),
              ]),
        ),
        SizedBox(
          height: 7.0,
        ),
        RichText(
          text: TextSpan(style: TextStyle(fontSize: 16), children: [
            TextSpan(
                text: currentCar.price.toString(),
                style: TextStyle(color: Colors.grey[20])),
            TextSpan(text: " / day", style: TextStyle(color: Colors.grey))
          ]),
        )
      ],
    );
  }
}

//Widget del carrusel de imagenes
class CarCarousel extends StatefulWidget {
  @override
  _CarCarouselState createState() => _CarCarouselState();
}

class _CarCarouselState extends State<CarCarousel> {
  static final List<String> imgList = currentCar.imgList;
  //Se crea la lista de imagenes que va a tener el carrusel
  final List<Widget> child = _map<Widget>(imgList, (index, String assetName) {
    return Container(
      child: Image.asset(
        "assets/$assetName",
        fit: BoxFit.fitWidth,
      ),
    );
  }).toList();

  static List<T> _map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }

  int _current = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          CarouselSlider(
            height: 250, //Altura del carrusel
            viewportFraction:
                1.0, //Porcentaje del carrusel que va a usar la imagen
            items: child, //Lista de items
            onPageChanged: (index) {
              setState(() {
                _current = index;
              });
            },
          ),
          //Linea que representa en que imagen del carrusel se esta
          Container(
            margin: EdgeInsets.only(left: 25),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: _map<Widget>(imgList, (index, assetName) {
                return Container(
                    width: 50,
                    height: 2,
                    decoration: BoxDecoration(
                        color: _current == index
                            ? Colors.grey[100]
                            : Colors.grey[700]));
              }),
            ),
          )
        ],
      ),
    );
  }
}

class CustomBottomSheet extends StatefulWidget {
  CustomBottomSheet({Key key}) : super(key: key);

  @override
  _CustomBottomSheetState createState() => _CustomBottomSheetState();
}

class _CustomBottomSheetState extends State<CustomBottomSheet>
    with SingleTickerProviderStateMixin {
  double sheetTop = 400;
  double minSheetTop = 15;
  double sheetItemHeight = 110;

  Animation<double> animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: Duration(milliseconds: 200), vsync: this);
    animation = Tween<double>(begin: sheetTop, end: minSheetTop)
        .animate(CurvedAnimation(
      parent: controller,
      curve: Curves.easeInOut,
      reverseCurve: Curves.easeInOut,
    ))
          ..addListener(() {
            setState(() {});
          });
  }

  forwardAnimation() {
    controller.forward();
    stateBloc.toggleAnimation();
  }

  reverseAnimation() {
    controller.reverse();
    stateBloc.toggleAnimation();
  }

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: animation.value,
      left: 0,
      child: GestureDetector(
        onTap: () {
          controller.isCompleted ? reverseAnimation() : forwardAnimation();
        },
        onVerticalDragEnd: (DragEndDetails dragEndDetails) {
          //Tiene esta velocidad cuando se arrastra el dedo de abajo para arriba
          if (dragEndDetails.primaryVelocity < 0.0) {
            forwardAnimation();
          } else if (dragEndDetails.primaryVelocity > 0.0) {
            reverseAnimation();
          } else {
            return;
          }
        },
        child: Container(
          padding: EdgeInsets.only(top: 25),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.vertical(top: Radius.circular(40)),
              color: Color(0xfff1f1f1)),
          child: Column(
            children: <Widget>[
              drawerHandle(),
              Expanded(
                flex: 1,
                child: ListView(
                  children: <Widget>[
                    offerDetails(sheetItemHeight),
                    specifications(sheetItemHeight),
                    features(sheetItemHeight),
                    SizedBox(height: 150),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  drawerHandle() {
    return Container(
      margin: EdgeInsets.only(bottom: 25),
      height: 3,
      width: 65,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15), color: Color(0xffd9bdbd)),
    );
  }

  specifications(double sheetItemHeight) {
    return Container(
      padding: EdgeInsets.only(top: 15, left: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('Specifications',
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontWeight: FontWeight.w700)),
          Container(
            margin: EdgeInsets.only(top: 15),
            height: sheetItemHeight,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: currentCar.specifications.length,
              itemBuilder: (context, index) {
                return specificationListItem(
                  sheetItemHeight,
                  currentCar.specifications[index],
                );
              },
            ),
          )
        ],
      ),
    );
  }

  features(double sheetItemHeight) {
    return Container(
      padding: EdgeInsets.only(top: 15, left: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('Features',
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontWeight: FontWeight.w700)),
          Container(
            margin: EdgeInsets.only(top: 15),
            height: sheetItemHeight,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: currentCar.features.length,
              itemBuilder: (context, index) {
                return listItem(
                  sheetItemHeight,
                  currentCar.features[index],
                );
              },
            ),
          )
        ],
      ),
    );
  }

  offerDetails(double sheetItemHeight) {
    return Container(
      padding: EdgeInsets.only(top: 15, left: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('Offer Details',
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontWeight: FontWeight.w700)),
          Container(
            margin: EdgeInsets.only(top: 15),
            height: sheetItemHeight,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: currentCar.offerDetails.length,
              itemBuilder: (context, index) {
                return listItem(
                  sheetItemHeight,
                  currentCar.offerDetails[index],
                );
              },
            ),
          )
        ],
      ),
    );
  }

  listItem(double sheetItemHeight, Map mapval) {
    return Container(
      margin: EdgeInsets.only(right: 20),
      width: sheetItemHeight,
      height: sheetItemHeight,
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          mapval.keys.elementAt(0),
          FittedBox(
            child: Text(
              mapval.values.elementAt(0),
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w600,
                  fontSize: 15),
            ),
          )
        ],
      ),
    );
  }

  specificationListItem(double sheetItemHeight, Map mapval) {
    Map subMap = mapval.values.elementAt(0);
    return Container(
      margin: EdgeInsets.only(right: 20),
      width: sheetItemHeight,
      height: sheetItemHeight,
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          mapval.keys.elementAt(0),
          FittedBox(
            child: Text(
              subMap.keys.elementAt(0),
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w700,
                  fontSize: 15),
            ),
          ),
          FittedBox(
            child: Text(
              subMap.values.elementAt(0),
              style: TextStyle(color: Colors.black, fontSize: 15),
            ),
          )
        ],
      ),
    );
  }
}

class UsePromotionButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomRight,
      child: SizedBox(
        width: 200,
        child: FlatButton(
          onPressed: () {
            showDialog(
              barrierDismissible: false,
              context: context,
              builder: (_) => QRCode(),
            );
          },
          child: Text(
            "Use Promotion",
            style: TextStyle(
              color: Colors.white,
              fontSize: 18,
              letterSpacing: 1.4,
            ),
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(topLeft: Radius.circular(35)),
          ),
          color: Colors.black,
          padding: EdgeInsets.all(25),
          
        ),
      ),
    );
  }
}

class QRCode extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => QRCodeState();
}

class QRCodeState extends State<QRCode> with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;

  forward() {
    controller.forward();
  }

  reverse() {
    controller.reverse();
  }

  @override
  void initState() {
    super.initState();

    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 350));
    scaleAnimation = CurvedAnimation(
        parent: controller,
        curve: Curves.easeInOut,
        reverseCurve: Curves.easeInOut);

    controller.addListener(() {
      setState(() {});
    });

    forward();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Material(
      color: Colors.transparent,
      child: ScaleTransition(
          scale: scaleAnimation,
          child: Container(
            height: MediaQuery.of(context).size.height * 0.4,
            width: MediaQuery.of(context).size.width * 0.8,
            padding: const EdgeInsets.all(10),
            margin: const EdgeInsets.all(5),
            decoration: BoxDecoration(
              border: Border.all(
                width: 4,
                color: Colors.grey[800],
              ),
                color: Colors.white,
                borderRadius: BorderRadius.circular(40),
                boxShadow: <BoxShadow>[
                  BoxShadow(spreadRadius: 2.0, color: Colors.grey[800], blurRadius: 2.0)
                ]),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Text(
                  'To get the discount go to the store and show this QR Code',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                    letterSpacing: 1.4,
                  ),
                  textAlign: TextAlign.center,
                ),
                QrImage(
                  data: 'Código para generar el QR',
                  gapless: true,
                  size: 200,
                  errorCorrectionLevel: QrErrorCorrectLevel.H,
                ),
                FlatButton(
                    child: Text('Cancelar',
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                          letterSpacing: 1.4,
                        ),
                        textAlign: TextAlign.center),
                    onPressed: () {
                      reverse();
                      Navigator.of(context).pop();
                    })
              ],
            ),
          )),
    ));
  }
}
