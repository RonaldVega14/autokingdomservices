class StateProvider {
  bool isAnimating = true;
  void toggleAnimationValue() => isAnimating = !isAnimating;

  bool isFav = false;
  void toggleFavoriteValue() => isFav = !isFav;
}
