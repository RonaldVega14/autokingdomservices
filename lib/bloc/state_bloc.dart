import 'dart:async';
import 'state_provider.dart';

class StateBloc {
  StreamController animationController = StreamController();
  StreamController favoriteController = StreamController();
  final StateProvider provider = StateProvider();

  Stream get animationStatus => animationController.stream;
  Stream get favoriteStatus => favoriteController.stream;

  void toggleAnimation() {
    provider.toggleAnimationValue();
    animationController.sink.add(provider.isAnimating);
  }

  void toggleFavorite() {
    provider.toggleFavoriteValue();
    favoriteController.sink.add(provider.isFav);
  }

  void dispone() {
    animationController.close();
    favoriteController.close();
  }
}

final stateBloc = StateBloc();
